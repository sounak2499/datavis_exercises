import { writable } from "svelte/store";

export function network_hover(nodes, links) {
  // Computes the neighbors of each node (WeakMap allows objects as keys)
  const neighbors = links.reduce((agg, l) => {
    agg.set(l.target, [l.source, ...(agg.get(l.target) || [])]);
    agg.set(l.source, [l.target, ...(agg.get(l.source) || [])]);
    return agg;
  }, new WeakMap());

  // The currently hovered node
  const hover_node = writable(null);

  // The output
  return {
    reset() {
      hover_node.set(null);
    },
    activate(node) {
      hover_node.set(node);
    }
  };
}
